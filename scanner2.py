import argparse
import pexpect
import xml.etree.ElementTree as ET
import time
import json
import shlex
import subprocess
from prettytable import PrettyTable  # New import for table display
# Mapping of short names to config IDs and descriptions
configs = {
    'base': ('d21f6c81-2b88-4ac1-b7b4-a2a9f2ad4663', 'Basic configuration template with a minimum set of NVTs required for a scan.'),
    'discovery': ('8715c877-47a0-438d-98a3-27c7a6ab2196', 'Network Discovery scan configuration.'),
    'empty': ('085569ce-73ed-11df-83c3-002264764cea', 'Empty and static configuration template.'),
    'euleros': ('0362e8f6-d7cc-4a12-8768-5f2406713860', 'Check compliance status of EulerOS installation against policy.'),
    'full_fast': ('daba56c8-73ec-11df-a475-002264764cea', 'Most NVTs; optimized by using previously collected information.'),
    'gaussdb100': ('61327f09-8a54-4854-9e1c-16798285fb28', 'Check compliance status of GaussDB installation against policy.'),
    'gaussdb_kernel': ('2eec8313-fee4-442a-b3c4-fa0d5dc83d61', 'Check compliance status against GaussDB Kernel policy.'),
    'host_discovery': ('2d3f051c-55ba-11e3-bf43-406186ea4fc5', 'Network Host Discovery scan configuration.'),
    'huawei_datacom': ('aab5c4a1-eab1-4f4e-acac-8c36d08de6bc', 'Check compliance status of Huawei Datacom Device against policy.'),
    'it_grundschutz': ('c4b7c0cb-6502-4809-b034-8e635311b3e6', 'Policy for IT-Grundschutz Kompendium.')
}

# Scanners dictionary
scanners = {
    'normal': '08b69003-5fc2-4037-a479-93b440211c73',
    'cve': '6acd0832-df90-11e4-b9d5-28d24461215b'
}

def initial_setup_needed():
    # Check if 'vagrant' is in the '_gvm' group
    check_user = subprocess.run(['getent', 'group', '_gvm'], capture_output=True, text=True)
    if 'vagrant' not in check_user.stdout:
        return True
    return False

def perform_initial_setup():
    if initial_setup_needed():
        # List of commands to setup environment
        setup_commands = [
            ['sudo', 'systemctl', 'stop', 'postgresql'],
            ['sudo', 'pg_lsclusters'],
            ['sudo', 'pg_dropcluster', '15', 'main', '--stop'],
            # Modify postgresql.conf - This requires editing a file which can't be directly done with subprocess
            ['sudo', 'sed', '-i', '/^port = /c\port = 5432', '/etc/postgresql/16/main/postgresql.conf'],
            ['sudo', 'systemctl', 'restart', 'postgresql@16-main'],
            ['sudo', 'gvm-setup'],
            ['sudo', 'runuser', '-u', '_gvm', '--', 'greenbone-feed-sync', '--type', 'SCAP'],
            ['sudo', 'usermod', '-aG', '_gvm', 'vagrant'],
            ['sudo', 'runuser', '-u', '_gvm', '--', 'gvmd', '--create-user=huntdown', '--password=huntdown']
        ]

        for command in setup_commands:
            subprocess.run(command, check=True)

        print("Initial setup completed.")
    else:
        print("Setup not required, proceeding with the script.")

def execute_gvm_command(xml_command):
    command = f'gvm-cli socket --xml {shlex.quote(xml_command)}'


    child = pexpect.spawn(command, encoding='utf-8', timeout=120)
    child.expect('Enter username:')
    child.sendline('huntdown')
    child.expect('Enter password for huntdown:')
    child.sendline('huntdown')
    child.expect(pexpect.EOF)
    response = child.before.strip()
    return response

def create_port_list(port_range):
    xml_command = f'<create_port_list><name>All Ports List</name><port_range>{port_range}</port_range></create_port_list>'
    output = execute_gvm_command(xml_command)
    if "Response Error 400. Port list exists already" in output:
        return get_existing_port_list_id(port_range)
    try:
        root = ET.fromstring(output)
        port_list_id = root.get('id')
        return {'port_list_id': port_list_id}
    except ET.ParseError:
        return {"error": f"Error creating port list: {output}"}

def get_existing_port_list_id(port_range):
    output = execute_gvm_command('<get_port_lists/>')
    root = ET.fromstring(output)
    for port_list in root.findall('.//port_list'):
        name = port_list.find('name').text
        if f"All Ports List" in name:
            port_list_id = port_list.get('id')
            return {'port_list_id': port_list_id}
    return {"error": f"Port list for all ports exists but could not find ID."}

def get_port_list_id(port_range):
    output = execute_gvm_command('<get_port_lists/>')
    root = ET.fromstring(output)
    for port_list in root.findall('.//port_list'):
        name = port_list.find('name').text
        if f"All Ports List" in name:
            port_list_id = port_list.get('id')
            return {'port_list_id': port_list_id}
    return create_port_list(port_range)

def get_latest_task_report_id(task_id):
    """
    Find the most recent report ID for a task matching `task_id`.
    """
    output = execute_gvm_command(f'<get_tasks task_id="{task_id}"/>')
    root = ET.fromstring(output)

    # Find the report ID in the task response
    report_element = root.find('.//last_report/report')
    if report_element is not None:
        report_id = report_element.get('id')
        return report_id  # Return the report ID if found
    else:
        return None  # Return None if no report is found


def check_for_existing_target(target_name):
    output = execute_gvm_command('<get_targets/>')
    try:
        root = ET.fromstring(output)
        for target in root.findall('.//target'):
            name = target.find('name').text
            if name == target_name:
                target_id = target.get('id')
                return {'target_id': target_id}
    except ET.ParseError:
        return {"error": f"Error checking for existing target: {output}"}
    return None

def create_target(target, port_list_id, username=None, password=None):
    ip, port = target.split(':')
    target_name = f'{ip}_{port}'
    existing_target_response = check_for_existing_target(target_name)
    if existing_target_response:
        return existing_target_response

    xml_command = f'<create_target><name>{target_name}</name><hosts>{ip}</hosts><port_list id="{port_list_id}"/>'
    if username and password:
        xml_command += f'<credentials><credential><name>SSH Credentials</name><login>{username}</login><password>{password}</password></credential></credentials>'
    xml_command += '</create_target>'
    output = execute_gvm_command(xml_command)
    try:
        root = ET.fromstring(output)
        target_id = root.get('id')
        return {'target_id': target_id}
    except ET.ParseError:
        return {"error": f"Error creating target: {output}"}

def start_scan(target, scan_type, config_type, output_file, username=None, password=None):
    ip, port = target.split(':')
    target_name = f'{ip}_{port}'
    port_list_response = get_port_list_id(port)
    if 'error' in port_list_response:
        return port_list_response
    
    target_response = create_target(target, port_list_response['port_list_id'], username, password)
    if 'error' in target_response:
        return target_response

    if config_type:
        config_id = configs[config_type][0]
    else:
        config_id = None

    scanner_id = scanners[scan_type]
    xml_command = f'<create_task><name>Scan {target_name}</name>'
    if config_id:
        xml_command += f'<config id="{config_id}"/>'
    xml_command += f'<target id="{target_response["target_id"]}"/><scanner id="{scanner_id}"/></create_task>'
    
    output = execute_gvm_command(xml_command)
    try:
        root = ET.fromstring(output)
        task_id = root.get('id')
        xml_command = f'<start_task task_id="{task_id}"/>'
        execute_gvm_command(xml_command)
        return monitor_scan(task_id, output_file)
    except ET.ParseError:
        return {"error": f"Error starting scan: {output}"}

def monitor_scan(task_id, output_file):
    while True:
        output = execute_gvm_command(f'<get_tasks task_id="{task_id}"/>')
        root = ET.fromstring(output)
        
        # Add this log for status checks
        status = root.find('.//status').text
        #print(f"Checking task status... Current status: {status}")
        
        if status == 'Done':
            #print("Scan completed! Fetching report...")
            return fetch_scan_report(task_id, output_file)
        elif status in ['Stopped', 'Error']:
            print(f"Scan encountered an error or was stopped. Status: {status}")
            return {"error": f"Scan stopped with status: {status}"}
        else:
            time.sleep(10)  # Reduced to 10 seconds


def print_table_from_json_report(json_report):
    """
    Extract crucial parts from the JSON report and print them as a table.
    """
    if not json_report or 'get_reports_response' not in json_report:
        print("Invalid JSON report")
        return

    report_data = json_report['get_reports_response'].get('report', {})

    # Extracting necessary data
    task_name = report_data.get('task', {}).get('name', 'N/A')
    total_vulns = report_data.get('report', {}).get('vulns', {}).get('count', '0')
    os_count = report_data.get('report', {}).get('os', {}).get('count', '0')
    hosts_count = report_data.get('report', {}).get('hosts', {}).get('count', '0')

    # Creating a table using PrettyTable
    table = PrettyTable()
    table.field_names = ["Task Name", "Total Vulnerabilities", "OS Count", "Hosts Count"]
    table.add_row([task_name, total_vulns, os_count, hosts_count])

    # Print the table
    print("\nSummary Table:")
    print(table)

    # Print the full JSON report
    print("\nFull JSON Report:")
    print(json.dumps(json_report, indent=4))

def fetch_scan_report(task_name, output_file):
    """
    Fetch the latest scan report based on the task name and print the table with the report.
    """
    report_id = get_latest_task_report_id(task_name)
    if report_id:
        # Fetch the report using the report_id
        report_output = execute_gvm_command(f'<get_reports report_id="{report_id}" details="1"/>')
        
        # Save the report in JSON format
        json_output = xml_to_json(report_output)
        json_str = json.dumps(json_output, indent=4)

        with open(output_file, 'w') as json_file:
            json_file.write(json_str)
        #print(f"Scan report saved to {output_file}")

        # Print crucial parts of the report in a table
        print_table_from_json_report(json_output)

        return json_output
    else:
        print(f"No completed task found with the name: {task_name}")
        return None
def xml_to_json(xml_str):
    root = ET.fromstring(xml_str)
    return elem_to_dict(root)

def elem_to_dict(elem):
    d = {elem.tag: {} if elem.attrib else None}
    children = list(elem)
    if children:
        dd = {}
        for dc in map(elem_to_dict, children):
            for k, v in dc.items():
                if k in dd:
                    if not isinstance(dd[k], list):
                        dd[k] = [dd[k]]
                    dd[k].append(v)
                else:
                    dd[k] = v
        d = {elem.tag: dd}
    if elem.attrib:
        d[elem.tag].update(('@' + k, v) for k, v in elem.attrib.items())
    if elem.text:
        text = elem.text.strip()
        if children or elem.attrib:
            if text:
                d[elem.tag]['#text'] = text
        else:
            d[elem.tag] = text
    return d

def list_combinations():
    print("Available Scanners:")
    for key, value in scanners.items():
        print(f"  {key}: {value}")
    
    print("\nAvailable Configurations:")
    for key, value in configs.items():
        print(f"  {key}: {value[1]}")
    
    print("\nAvailable Scan Combinations:")
    for scan_key in scanners.keys():
        for config_key in configs.keys():
            print(f"  Scanner: {scan_key}, Config: {config_key}")

def main():
    parser = argparse.ArgumentParser(description="GVM script to handle port list creation, target creation, and scan initiation.")
    parser.add_argument('-scan', help="Scan in the format 'ip:port scan_type config_type' for scan initiation")
    parser.add_argument('-output', help="File to save the JSON output. If not provided, defaults to 'scan_report.json'", required=False)
    args = parser.parse_args()

    # Check if scan parameters are provided and proceed with scanning
    if hasattr(args, 'scan') and args.scan:
        scan_params = args.scan.split(' ')
        if len(scan_params) == 3:
            target, scan_type, config_type = scan_params
        elif len(scan_params) == 2:
            target, scan_type = scan_params
            config_type = None
        else:
            print("Error: You must provide at least 'ip:port' and 'scan_type'.")
            return

        if scan_type not in scanners:
            print(f"Invalid scan type: {scan_type}. Use -list to see available options.")
            return
        if config_type and config_type not in configs:
            print(f"Invalid config type: {config_type}. Use -list to see available options.")
            return

        # If the user does not provide an output, set a default file name
        output_file = args.output if args.output else 'scan_report.json'

        # Proceed with the scan
        json_output = start_scan(target, scan_type, config_type, output_file)
        json_str = json.dumps(json_output, indent=4)
        print(json_str)

if __name__ == "__main__":
    main()
