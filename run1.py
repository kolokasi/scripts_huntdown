import argparse
import subprocess
import xml.etree.ElementTree as ET
import shlex
import pexpect
import time
import json
import os

def execute_gvm_command(xml_command):
    command = f'gvm-cli socket --xml {shlex.quote(xml_command)}'
    child = pexpect.spawn(command, encoding='utf-8', timeout=120)
    child.expect('Enter username:')
    child.sendline('huntdown')
    child.expect('Enter password for huntdown:')
    child.sendline('huntdown')
    child.expect(pexpect.EOF)
    return child.before.strip()

def create_port_list(port):
    xml_command = f'<create_port_list><name>Port {port} List</name><port_range>{port}</port_range></create_port_list>'
    output = execute_gvm_command(xml_command)
    try:
        root = ET.fromstring(output)
        port_list_id = root.get('id')
        return {'port_list_id': port_list_id}
    except ET.ParseError:
        return {"error": f"Error creating port list: {output}"}

def get_port_list_id(port):
    output = execute_gvm_command('<get_port_lists/>')
    root = ET.fromstring(output)
    for port_list in root.findall('.//port_list'):
        name = port_list.find('name').text
        if f"Port {port} List" in name:
            return {'port_list_id': port_list.get('id')}
    return create_port_list(port)

def check_for_existing_target(target_name):
    output = execute_gvm_command('<get_targets/>')
    try:
        root = ET.fromstring(output)
        for target in root.findall('.//target'):
            name = target.find('name').text
            if name == target_name:
                return {'target_id': target.get('id')}
    except ET.ParseError:
        return {"error": f"Error checking for existing target: {output}"}
    return None

def create_target(target, port_list_id):
    ip, port = target.split(':')
    target_name = f'{ip}_{port}'
    existing_target_response = check_for_existing_target(target_name)
    if existing_target_response:
        return existing_target_response

    xml_command = f'<create_target><name>{target_name}</name><hosts>{ip}</hosts><port_list id="{port_list_id}"/></create_target>'
    output = execute_gvm_command(xml_command)
    try:
        root = ET.fromstring(output)
        target_id = root.get('id')
        return {'target_id': target_id}
    except ET.ParseError:
        return {"error": f"Error creating target: {output}"}

def start_scan(target, scan_type, output_file):
    scanner_ids = {
        'normal': '08b69003-5fc2-4037-a479-93b440211c73',
        'cve': '6acd0832-df90-11e4-b9d5-28d24461215b'
    }
    ip, port = target.split(':')
    target_name = f'{ip}_{port}'
    port_list_response = get_port_list_id(port)
    if 'error' in port_list_response:
        return port_list_response
    
    target_response = create_target(target, port_list_response['port_list_id'])
    if 'error' in target_response:
        return target_response

    scan_config_id = 'daba56c8-73ec-11df-a475-002264764cea'
    scanner_id = scanner_ids[scan_type]
    xml_command = f'<create_task><name>Scan {target_name}</name><config id="{scan_config_id}"/><target id="{target_response["target_id"]}"/><scanner id="{scanner_id}"/></create_task>'
    output = execute_gvm_command(xml_command)
    try:
        root = ET.fromstring(output)
        task_id = root.get('id')
        # Now start the scan
        xml_command = f'<start_task task_id="{task_id}"/>'
        start_output = execute_gvm_command(xml_command)
        monitor_output = monitor_scan(task_id, output_file)
        return monitor_output
    except ET.ParseError:
        return {"error": f"Error starting scan: {output}"}

def monitor_scan(task_id, output_file):
    while True:
        output = execute_gvm_command(f'<get_tasks task_id="{task_id}"/>')
        root = ET.fromstring(output)
        status = root.find('.//status').text
        if status == 'Done':
            return fetch_scan_report(task_id, output_file)
        elif status in ['Stopped', 'Error']:
            return {"error": f"Scan stopped with status: {status}"}
        else:
            time.sleep(60)

def fetch_scan_report(task_id, output_file):
    output = execute_gvm_command(f'<get_reports task_id="{task_id}"/>')
    root = ET.fromstring(output)
    report_id = root.find('.//report').get('id')
    report_output = execute_gvm_command(f'<get_reports report_id="{report_id}"/>')

    # Save JSON format
    json_output = xml_to_json(report_output)
    json_str = json.dumps(json_output, indent=4)

    # Save JSON output to the specified file
    with open(output_file, 'w') as json_file:
        json_file.write(json_str)

    return json_output

def xml_to_json(xml_str):
    root = ET.fromstring(xml_str)
    return elem_to_dict(root)

def elem_to_dict(elem):
    d = {elem.tag: {} if elem.attrib else None}
    children = list(elem)
    if children:
        dd = {}
        for dc in map(elem_to_dict, children):
            for k, v in dc.items():
                if k in dd:
                    if not isinstance(dd[k], list):
                        dd[k] = [dd[k]]
                    dd[k].append(v)
                else:
                    dd[k] = v
        d = {elem.tag: dd}
    if elem.attrib:
        d[elem.tag].update(('@' + k, v) for k, v in elem.attrib.items())
    if elem.text:
        text = elem.text.strip()
        if children or elem.attrib:
            if text:
                d[elem.tag]['#text'] = text
        else:
            d[elem.tag] = text
    return d

def main():
    parser = argparse.ArgumentParser(description="GVM script to handle port list creation, target creation, and scan initiation.")
    parser.add_argument('-portlist', help="Port or port range for port list creation")
    parser.add_argument('-target', help="Target in the format ip:port for target creation")
    parser.add_argument('-scan', help="Scan in the format 'ip:port scan_type' for scan initiation")
    parser.add_argument('-output', help="File to save the JSON output")

    args = parser.parse_args()

    json_output = {}
    
    if args.portlist:
        json_output = create_port_list(args.portlist)
    
    if args.target:
        port_list_id_response = get_port_list_id(args.target.split(':')[1])
        if 'error' in port_list_id_response:
            json_output = port_list_id_response
        else:
            json_output = create_target(args.target, port_list_id_response['port_list_id'])
    
    if args.scan:
        target, scan_type = args.scan.split(' ')
        json_output = start_scan(target, scan_type, args.output)
    
    json_str = json.dumps(json_output, indent=4)
    print(json_str)

    if args.output:
        with open(args.output, 'w') as json_file:
            json_file.write(json_str)

if __name__ == "__main__":
    main()
