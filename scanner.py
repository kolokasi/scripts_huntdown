import argparse
import pexpect
import xml.etree.ElementTree as ET
import time
import json
import shlex

# Mapping of short names to config IDs and descriptions
configs = {
    'base': ('d21f6c81-2b88-4ac1-b7b4-a2a9f2ad4663', 'Basic configuration template with a minimum set of NVTs required for a scan.'),
    'discovery': ('8715c877-47a0-438d-98a3-27c7a6ab2196', 'Network Discovery scan configuration.'),
    'empty': ('085569ce-73ed-11df-83c3-002264764cea', 'Empty and static configuration template.'),
    'euleros': ('0362e8f6-d7cc-4a12-8768-5f2406713860', 'Check compliance status of EulerOS installation against policy.'),
    'full_fast': ('daba56c8-73ec-11df-a475-002264764cea', 'Most NVTs; optimized by using previously collected information.'),
    'gaussdb100': ('61327f09-8a54-4854-9e1c-16798285fb28', 'Check compliance status of GaussDB installation against policy.'),
    'gaussdb_kernel': ('2eec8313-fee4-442a-b3c4-fa0d5dc83d61', 'Check compliance status against GaussDB Kernel policy.'),
    'host_discovery': ('2d3f051c-55ba-11e3-bf43-406186ea4fc5', 'Network Host Discovery scan configuration.'),
    'huawei_datacom': ('aab5c4a1-eab1-4f4e-acac-8c36d08de6bc', 'Check compliance status of Huawei Datacom Device against policy.'),
    'it_grundschutz': ('c4b7c0cb-6502-4809-b034-8e635311b3e6', 'Policy for IT-Grundschutz Kompendium.')
}

# Scanners dictionary
scanners = {
    'normal': '08b69003-5fc2-4037-a479-93b440211c73',
    'cve': '6acd0832-df90-11e4-b9d5-28d24461215b'
}

def execute_gvm_command(xml_command):
    command = f'gvm-cli socket --xml {shlex.quote(xml_command)}'
    child = pexpect.spawn(command, encoding='utf-8', timeout=120)
    child.expect('Enter username:')
    child.sendline('admin')
    child.expect('Enter password for admin:')
    child.sendline('kali')
    child.expect(pexpect.EOF)
    response = child.before.strip()
    return response

def create_port_list(port_range):
    xml_command = f'<create_port_list><name>All Ports List</name><port_range>{port_range}</port_range></create_port_list>'
    output = execute_gvm_command(xml_command)
    if "Response Error 400. Port list exists already" in output:
        return get_existing_port_list_id(port_range)
    try:
        root = ET.fromstring(output)
        port_list_id = root.get('id')
        return {'port_list_id': port_list_id}
    except ET.ParseError:
        return {"error": f"Error creating port list: {output}"}

def get_existing_port_list_id(port_range):
    output = execute_gvm_command('<get_port_lists/>')
    root = ET.fromstring(output)
    for port_list in root.findall('.//port_list'):
        name = port_list.find('name').text
        if f"All Ports List" in name:
            port_list_id = port_list.get('id')
            return {'port_list_id': port_list_id}
    return {"error": f"Port list for all ports exists but could not find ID."}

def get_port_list_id(port_range):
    output = execute_gvm_command('<get_port_lists/>')
    root = ET.fromstring(output)
    for port_list in root.findall('.//port_list'):
        name = port_list.find('name').text
        if f"All Ports List" in name:
            port_list_id = port_list.get('id')
            return {'port_list_id': port_list_id}
    return create_port_list(port_range)

def check_for_existing_target(target_name):
    output = execute_gvm_command('<get_targets/>')
    try:
        root = ET.fromstring(output)
        for target in root.findall('.//target'):
            name = target.find('name').text
            if name == target_name:
                target_id = target.get('id')
                return {'target_id': target_id}
    except ET.ParseError:
        return {"error": f"Error checking for existing target: {output}"}
    return None

def create_target(target, port_list_id, username=None, password=None):
    ip, port = target.split(':')
    target_name = f'{ip}_{port}'
    existing_target_response = check_for_existing_target(target_name)
    if existing_target_response:
        return existing_target_response

    xml_command = f'<create_target><name>{target_name}</name><hosts>{ip}</hosts><port_list id="{port_list_id}"/>'
    if username and password:
        xml_command += f'<credentials><credential><name>SSH Credentials</name><login>{username}</login><password>{password}</password></credential></credentials>'
    xml_command += '</create_target>'
    output = execute_gvm_command(xml_command)
    try:
        root = ET.fromstring(output)
        target_id = root.get('id')
        return {'target_id': target_id}
    except ET.ParseError:
        return {"error": f"Error creating target: {output}"}

def start_scan(target, scan_type, config_type, output_file, username=None, password=None):
    ip, port = target.split(':')
    target_name = f'{ip}_{port}'
    port_list_response = get_port_list_id(port)
    if 'error' in port_list_response:
        return port_list_response
    
    target_response = create_target(target, port_list_response['port_list_id'], username, password)
    if 'error' in target_response:
        return target_response

    if config_type:
        config_id = configs[config_type][0]
    else:
        config_id = None

    scanner_id = scanners[scan_type]
    xml_command = f'<create_task><name>Scan {target_name}</name>'
    if config_id:
        xml_command += f'<config id="{config_id}"/>'
    xml_command += f'<target id="{target_response["target_id"]}"/><scanner id="{scanner_id}"/></create_task>'
    
    output = execute_gvm_command(xml_command)
    try:
        root = ET.fromstring(output)
        task_id = root.get('id')
        xml_command = f'<start_task task_id="{task_id}"/>'
        execute_gvm_command(xml_command)
        return monitor_scan(task_id, output_file)
    except ET.ParseError:
        return {"error": f"Error starting scan: {output}"}

def monitor_scan(task_id, output_file):
    while True:
        output = execute_gvm_command(f'<get_tasks task_id="{task_id}"/>')
        root = ET.fromstring(output)
        status = root.find('.//status').text
        if status == 'Done':
            return fetch_scan_report(task_id, output_file)
        elif status in ['Stopped', 'Error']:
            return {"error": f"Scan stopped with status: {status}"}
        else:
            time.sleep(60)

def fetch_scan_report(task_id, output_file):
    output = execute_gvm_command(f'<get_reports task_id="{task_id}"/>')
    root = ET.fromstring(output)
    report_id = root.find('.//report').get('id')
    report_output = execute_gvm_command(f'<get_reports report_id="{report_id}"/>')

    # Save XML format
    xml_output_file = output_file.replace('.json', '.xml')
    with open(xml_output_file, 'w') as xml_file:
        xml_file.write(report_output)
    print(f"Scan report saved to {xml_output_file}")

    # Save JSON format
    json_output = xml_to_json(report_output)
    json_str = json.dumps(json_output, indent=4)

    with open(output_file, 'w') as json_file:
        json_file.write(json_str)
    print(f"Scan report saved to {output_file}")

    return json_output

def xml_to_json(xml_str):
    root = ET.fromstring(xml_str)
    return elem_to_dict(root)

def elem_to_dict(elem):
    d = {elem.tag: {} if elem.attrib else None}
    children = list(elem)
    if children:
        dd = {}
        for dc in map(elem_to_dict, children):
            for k, v in dc.items():
                if k in dd:
                    if not isinstance(dd[k], list):
                        dd[k] = [dd[k]]
                    dd[k].append(v)
                else:
                    dd[k] = v
        d = {elem.tag: dd}
    if elem.attrib:
        d[elem.tag].update(('@' + k, v) for k, v in elem.attrib.items())
    if elem.text:
        text = elem.text.strip()
        if children or elem.attrib:
            if text:
                d[elem.tag]['#text'] = text
        else:
            d[elem.tag] = text
    return d

def list_combinations():
    print("Available Scanners:")
    for key, value in scanners.items():
        print(f"  {key}: {value}")
    
    print("\nAvailable Configurations:")
    for key, value in configs.items():
        print(f"  {key}: {value[1]}")
    
    print("\nAvailable Scan Combinations:")
    for scan_key in scanners.keys():
        for config_key in configs.keys():
            print(f"  Scanner: {scan_key}, Config: {config_key}")

def main():
    parser = argparse.ArgumentParser(description="GVM script to handle port list creation, target creation, and scan initiation.")
    parser.add_argument('-scan', help="Scan in the format 'ip:port scan_type config_type' for scan initiation")
    parser.add_argument('-output', help="File to save the JSON output")
    parser.add_argument('-list', action='store_true', help="List available scanners, configurations, and their combinations")
    parser.add_argument('-user', '--username', help="Username for target authentication")
    parser.add_argument('-pass', '--password', help="Password for target authentication")

    args = parser.parse_args()

    if args.list:
        list_combinations()
        return

    if args.scan:
        scan_params = args.scan.split(' ')
        if len(scan_params) == 3:
            target, scan_type, config_type = scan_params
        elif len(scan_params) == 2:
            target, scan_type = scan_params
            config_type = None
        else:
            print("Error: You must provide at least 'ip:port' and 'scan_type'.")
            return
        
        if scan_type not in scanners:
            print(f"Invalid scan type: {scan_type}. Use -list to see available options.")
            return
        if config_type and config_type not in configs:
            print(f"Invalid config type: {config_type}. Use -list to see available options.")
            return

        if not args.output:
            print("Output file is required when running a scan. Use -output to specify the file.")
            return

        json_output = start_scan(target, scan_type, config_type, args.output, args.username, args.password)
        
        json_str = json.dumps(json_output, indent=4)
        print(json_str)

        if args.output:
            with open(args.output, 'w') as json_file:
                json_file.write(json_str)

if __name__ == "__main__":
    main()
