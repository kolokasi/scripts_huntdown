import argparse
import subprocess
import xml.etree.ElementTree as ET
import shlex
import pexpect
import time
import os

def is_gvm_running():
    # Using pgrep to check if gvmd (GVM daemon) is running
    result = subprocess.run(['pgrep', 'gvmd'], stdout=subprocess.PIPE, text=True)
    return result.returncode == 0

# Function to start GVM using pexpect to handle password input
def start_gvm():
    child = pexpect.spawn('sudo gvm-start')
    child.expect('password for .*:')
    child.sendline('kali')
    child.expect(pexpect.EOF)

# At the beginning of your main() function
if not is_gvm_running():
    print("GVM services are not running. Starting GVM services...")
    start_gvm()
    time.sleep(10)  # Wait for services to start
    if not is_gvm_running():
        print("Failed to start GVM services.")
        exit(1)
    else:
        print("GVM services started successfully.")

def execute_gvm_command(xml_command):
    command = f'gvm-cli socket --xml {shlex.quote(xml_command)}'
    child = pexpect.spawn(command, encoding='utf-8', timeout=120)
    child.expect('Enter username:')
    child.sendline('admin')
    child.expect('Enter password for admin:')
    child.sendline('kali')
    child.expect(pexpect.EOF)
    return child.before.strip()

def create_port_list(port):
    xml_command = f'<create_port_list><name>Port {port} List</name><port_range>{port}</port_range></create_port_list>'
    output = execute_gvm_command(xml_command)
    try:
        root = ET.fromstring(output)
        return root.get('id')
    except ET.ParseError:
        print(f"Error creating port list: {output}")
        return None

def get_port_list_id(port):
    output = execute_gvm_command('<get_port_lists/>')
    root = ET.fromstring(output)
    for port_list in root.findall('.//port_list'):
        name = port_list.find('name').text
        if f"Port {port} List" in name:
            return port_list.get('id')
    return create_port_list(port)

def check_for_existing_target(target_name):
    """Check if a target already exists and return its ID if it does."""
    output = execute_gvm_command('<get_targets/>')
    try:
        root = ET.fromstring(output)
        for target in root.findall('.//target'):
            name = target.find('name').text
            if name == target_name:
                return target.get('id')
    except ET.ParseError:
        print(f"Error checking for existing target: {output}")
    return None

def create_target(target, port_list_id):
    ip, port = target.split(':')
    target_name = f'{ip}_{port}'
    existing_target_id = check_for_existing_target(target_name)
    if existing_target_id:
        print(f"Target {target_name} already exists with ID: {existing_target_id}")
        return existing_target_id

    xml_command = f'<create_target><name>{target_name}</name><hosts>{ip}</hosts><port_list id="{port_list_id}"/></create_target>'
    output = execute_gvm_command(xml_command)
    try:
        root = ET.fromstring(output)
        target_id = root.get('id')
        print(f"Created target {target_name} with ID: {target_id}")
        return target_id
    except ET.ParseError:
        print(f"Error creating target: {output}")
        return None

def start_scan(target, scan_type):
    scanner_ids = {
        'normal': '08b69003-5fc2-4037-a479-93b440211c73',
        'cve': '6acd0832-df90-11e4-b9d5-28d24461215b'
    }
    ip, port = target.split(':')
    target_name = f'{ip}_{port}'
    port_list_id = get_port_list_id(port)
    target_id = create_target(target, port_list_id)
    scan_config_id = 'daba56c8-73ec-11df-a475-002264764cea'
    scanner_id = scanner_ids[scan_type]
    xml_command = f'<create_task><name>Scan {target_name}</name><config id="{scan_config_id}"/><target id="{target_id}"/><scanner id="{scanner_id}"/></create_task>'
    output = execute_gvm_command(xml_command)
    try:
        root = ET.fromstring(output)
        task_id = root.get('id')
        print(f"Scan task created with ID: {task_id}")
        # Now start the scan
        xml_command = f'<start_task task_id="{task_id}"/>'
        start_output = execute_gvm_command(xml_command)
        print(f"Scan started with task ID {task_id}")
        monitor_scan(task_id)
    except ET.ParseError:
        print(f"Error starting scan: {output}")

def monitor_scan(task_id):
    print("Monitoring scan...")
    while True:
        output = execute_gvm_command(f'<get_tasks task_id="{task_id}"/>')
        root = ET.fromstring(output)
        status = root.find('.//status').text
        if status == 'Done':
            print("Scan completed.")
            fetch_scan_report(task_id)
            break
        elif status in ['Stopped', 'Error']:
            print(f"Scan stopped with status: {status}")
            break
        else:
            print(f"Scan status: {status}")
            time.sleep(60)

def fetch_scan_report(task_id):
    output = execute_gvm_command(f'<get_reports task_id="{task_id}"/>')
    root = ET.fromstring(output)
    report_id = root.find('.//report').get('id')
    report_output = execute_gvm_command(f'<get_reports report_id="{report_id}"/>')
    with open(f'scan_report_{task_id}.xml', 'w') as file:
        file.write(report_output)
    print(f"Scan report saved to scan_report_{task_id}.xml")

def main():
   # First, check if GVM services are running
    if not is_gvm_running():
        print("GVM services are not running. Starting GVM services...")
        start_gvm()
        # Wait a little to ensure services are started
        time.sleep(10)
        if not is_gvm_running():
            print("Failed to start GVM services.")
            exit(1)  # Exit if GVM services did not start
    parser = argparse.ArgumentParser(description="GVM script to handle port list creation, target creation, and scan initiation.")
    parser.add_argument('-portlist', help="Port or port range for port list creation")
    parser.add_argument('-target', help="Target in the format ip:port for target creation")
    parser.add_argument('-scan', help="Scan in the format 'ip:port scan_type' for scan initiation")

    args = parser.parse_args()

    if args.portlist:
        port_list_id = create_port_list(args.portlist)
        print(f'Created port list with ID: {port_list_id}')
    if args.target:
        port_list_id = get_port_list_id(args.target.split(':')[1])
        if port_list_id:
            target_id = create_target(args.target, port_list_id)
            print(f'Created target with ID: {target_id}')
        else:
            print("Failed to create or retrieve port list.")
    if args.scan:
        target, scan_type = args.scan.split(' ')
        start_scan(target, scan_type)

if __name__ == "__main__":
    main()
